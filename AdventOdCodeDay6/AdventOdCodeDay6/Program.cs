﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOdCodeDay6
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllText(@"input.txt").Split(',');
            List<int> lanthernFishList = new List<int>();

            foreach (var item in input)
            {
                lanthernFishList.Add(int.Parse(item));
            }

            var totalAmountOfFish = GetTotalLanthernFish(lanthernFishList);

            Console.WriteLine(totalAmountOfFish.ToString());
        }

        public static int GetTotalLanthernFish(List<int> fishList)
        {
            var countDays = 0;
            var newFish = 0;

            while (countDays <= 80)
            {
                while(newFish != 0)
                {
                    fishList.Add(8);
                    newFish--;
                }

                for(int i = 0; i < fishList.Count; i++)
                {
                    var value = fishList[i];
                    if(value != 0)
                    {
                        fishList[i] = fishList[i] - 1;
                    }
                    else
                    {
                        fishList[i] = 6;
                        newFish++;
                    }
                }
                countDays++;
            }

            return fishList.Count;
        }

    }
}
