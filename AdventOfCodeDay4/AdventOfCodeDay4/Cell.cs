﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCodeDay4
{
    class Cell
    {
        public Cell(int number)
        {
            Number = number;
        }
        public int Number { get; set; }
        public bool IsMarked { get; set; }
        public void SetMarked()
        {
            IsMarked = true;
        }
    }
}
