﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCodeDay4
{
    class Board
    {
        public Board(List<string> rows)
        {
            createBoard(rows);

        }
        public List<List<Cell>> BoardList { get; set; } = new List<List<Cell>>();

        public bool IsBingo { get; set; }

        public void createBoard(List<string> rows)
        {
            foreach (string row in rows)
            {
                var rowCells = row.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries).Select(x => new Cell(Int32.Parse(x))).ToList();
                BoardList.Add(rowCells);
            }
        }
    }
}
