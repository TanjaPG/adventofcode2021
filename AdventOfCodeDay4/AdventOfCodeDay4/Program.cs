﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCodeDay4
{
    class Program
    {
        static void Main(string[] args)
        {
            var bingoRow = File.ReadLines(@"input.txt").First();
            
            List<Board> allBoards = new List<Board>();

            string[] bingoNumbers = bingoRow.Split(',');

            var line = File.ReadLines(@"input.txt").Skip(2).ToList();

            List<int> totalOfAllBingo = new List<int>();

            for (int i = 0; i <= line.Count; i += 6)
            {
                List<string> boardRows = line.Skip(i).Take(5).ToList();
                Board board = new Board(boardRows);
                allBoards.Add(board);
            }

            foreach (var item in bingoNumbers)
            {
               totalOfAllBingo.AddRange(NumberExist(item, allBoards));
            }

            var lastWinner = totalOfAllBingo.Last();

            Console.WriteLine(lastWinner.ToString());

        }

        public static List<int> NumberExist(string number, List<Board> theBoard)
        {
            List<int> theBingos = new List<int>();
            var total = 0;
            foreach(Board item in theBoard)
            {
                total = 0;

                if(!item.IsBingo)
                {
                    for (int i = 0; i < item.BoardList.Count; i++)
                    {
                        var cell = item.BoardList[i].FirstOrDefault(x => x.Number.ToString() == number);

                        if (cell != null)
                            cell.IsMarked = true;
                    }

                    var bingo = IsBingo(item);

                    if (bingo)
                    {
                        item.IsBingo = true;
                        var sum = SumUnmarked(item);
                        total = (sum * int.Parse(number));
                        theBingos.Add(total);
                        
                    }
                }
            }

            return theBingos;
           
        }

        public static bool IsBingo(Board board)
        {
            var counter = 0;
            bool isBingo = false;
            for (int i = 0; i < board.BoardList.Count; i++)
            {
                counter = 0;

                isBingo = board.BoardList[i].All(x => x.IsMarked == true);

                if (isBingo)
                    break;

                for (int ic = 0; ic < board.BoardList[i].Count; ic++)
                {
                    var cell = board.BoardList[ic][i];

                    if(cell.IsMarked)
                    {
                        counter++;
                    }

                    if (counter == 5)
                    {
                        isBingo = true;
                        return isBingo;
                    }
                }

            }
            return isBingo;
        }

        public static int SumUnmarked(Board board)
        {
            var unmarked = 0;
            for(int i = 0; i < board.BoardList.Count; i++)
            {
                unmarked += board.BoardList[i].FindAll(x => x.IsMarked == false).Sum(x => x.Number);
            }

            return unmarked;
        }
    }
}
