﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCodeDay7
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllText(@"input.txt").Split(',');
            List<int> positionList = new List<int>();

            foreach (var item in input)
            {
                positionList.Add(int.Parse(item));
            }

            var minValue = Part1(positionList);
            Console.WriteLine(minValue.ToString());

            var minValueMoreFuel = Part2(positionList);
            Console.WriteLine(minValueMoreFuel.ToString());

        }

        public static int Part1(List<int> positionList)
        {
            List<int> fuelList = new List<int>();
            var minValue = 0;
            var fuel = 0;

            var minPosition = positionList.Min();
            var maxPosition = positionList.Max();

            for (int ii = minPosition; ii <= maxPosition; ii++)
            {
                fuel = 0;
                for (int i = 0; i < positionList.Count; i++)
                {
                    fuel += Math.Abs(positionList[i] - ii);
                }

                fuelList.Add(fuel);
            }
            minValue = fuelList.Min();
            return minValue;
        }

        public static int Part2(List<int> positionList)
        {
            List<int> fuelList = new List<int>();
            var minValue = 0;
            var steps = 0;
            var fuel = 0;
            var minPosition = positionList.Min();
            var maxPosition = positionList.Max();

            for (int ii = minPosition; ii <= maxPosition; ii++)
            {
                fuel = 0;
                for (int i = 0; i < positionList.Count; i++)
                {
                    steps = Math.Abs(positionList[i] - ii);
                    while (steps != 0)
                    {
                        fuel += steps;
                        steps--;
                    }
                }
                fuelList.Add(fuel);
            }
            minValue = fuelList.Min();
            return minValue;
        }
    }
}
