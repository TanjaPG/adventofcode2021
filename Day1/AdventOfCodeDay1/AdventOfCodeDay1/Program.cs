﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCodeDay1
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllLines(@"input.txt");
            var measurementList = new List<int>();

            foreach (string line in input)
            {
                measurementList.Add(Int32.Parse(line));
            }

            var amountOfIncrease = CountIncrease(measurementList);
            Console.WriteLine(amountOfIncrease.ToString());

            var sumList = SumMeasurement(measurementList);
            var amountOfIncreaseOnSum = CountIncrease(sumList);
            Console.WriteLine(amountOfIncreaseOnSum.ToString());

        }
        public static int CountIncrease(List<int> measurementList)
        {
            var value = measurementList[0];
            var count = 0;

            foreach (int item in measurementList)
            {
                if (item > value)
                    count++;

                value = item;
            }

            return count;
        }

        public static List<int> SumMeasurement(List<int> measurementList)
        {
            var sumList = new List<int>();
           
            for (int i = 0; i <= measurementList.Count; i++)
            {
                var sum = measurementList.Skip(i).Take(3).Sum();
                sumList.Add(sum);
            }

            return sumList;
        }
    }
}
