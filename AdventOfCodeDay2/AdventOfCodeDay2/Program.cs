﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AdventOfCodeDay2
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllLines(@"Day2.txt");
            var mapList = new List<KeyValuePair<string, int>>();

            foreach (string line in input)
            {
                string[] words = line.Split(' ');
                mapList.Add(new KeyValuePair<string, int>(words[0], Int32.Parse(words[1])));
            }

            var sumOfPosition = GetSumOfPosition(mapList);

            Console.WriteLine(sumOfPosition.ToString());
        }

        public static int GetSumOfPosition(List<KeyValuePair<string, int>> mapList)
        {
            var horizontalPosition = 0;
            var depth = 0;
            var aim = 0;

            foreach (var item in mapList)
            {
                if(item.Key == "forward")
                {
                    horizontalPosition += item.Value;
                    depth += (aim * item.Value);
                }
                else if(item.Key == "down")
                {
                    aim += item.Value;
                }
                else
                {
                    aim -= item.Value;
                }

            }

            var total = depth * horizontalPosition;
            return total;
        }
    }
}
